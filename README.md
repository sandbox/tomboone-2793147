# Feeds Tamper Conditional Field Find and Replace #

This module contains a Feeds Tamper plugin that allows a user to search for a particular value in another field, and if matched, perform a find and replace on the current field.

For example, if one RSS feed used by an RSS feed importer displays author names as raw usernames, the tamper could be used to 1) identify imported posts whose URL or GUID field value begins with a particular string (e.g., http://www.blog.com/thisblog) and, if matched, then 2) search for a particular user name in the author field and replace it with the author's "real" name.

Currently the module requires a user to already know the field ID of the conditional field and enter it manually. The module also only currently matches the beginning string of the field ID value. Later versions may allow for other matching operators.

There is an Invert option, that allows a user to perform a find and replace on the current field on all NON-matching current field values. There is not yet an Invert option for the conditional field.